**Problem Description:**

You are in charge of selling online video advertising at a large media company. The job is to make sure that you make as much money as possible from ads running on your site. You can sell virtually an infinite amount of impressions (a display of an ad to a user) to a multitude of advertisers.

However, the limitation is that you cannot show more than 3 ads before each video clip on the site, or no one will visit the site. This upcoming month you therefore have a total ad inventory of 32 356 000 impressions available (meaning that you can display ads a maximum of 32 356 000 times).

Also, it’s not possible to sell an arbitrary number of impressions to a customer. Your customers buy impressions in chunks of different sizes called campaigns. 

For instance, the advertiser Acme Inc. always buys campaigns of size 2 000 000 impressions that are to be delivered within one month. If you cannot deliver the full 2 000 000 impressions in time, Acme won't pay you. On the other hand, you are able to sell each customer an arbitrary amount of campaigns, as long as you can deliver all impressions in each campaign.

The job is to choose how many campaigns to sell to what customers in order to maximize the revenue. 

For the upcoming month, your options are as follows:



| Customer | Impressions per campaign | Revenue per campaign |
| --- | --- | --- |
| Acme | 2 000 000 | 200 € |
| Lorem | 3 500 000 | 400 € |
| Ipsum | 2 300 000 | 210 € |
| Dolor | 8 000 000 | 730 € |
| SIT | 10 000 000 | 1000 € |
| Amet | 1 500 000 | 160 € |
| Marius | 1 000 000 | 100 € |



Write a program that will help you find the best possible mix of campaigns while fast enough to handle a variety of different scenarios. Accordingly, write a java program that takes a single argument on the command line. This argument must be the name of a file which contains the scenario for a month. This input file will always be formatted as follows:


&lt;monthly inventory>,

&lt;customer>, &lt;impressions per campaign>, &lt;price per campaign>

...

&lt;customer>, &lt;impressions per campaign>, &lt;price per campaign>


The monthly inventory will be a positive integer. The customer name will contain no spaces. The costs will be integers. The values will be separated by commas. The program should print the best possible mix of campaigns to sell to standard out in the following form:


&lt;customer>, &lt;number of campaigns to sell>, &lt;total impressions forcustomer>, &lt;total revenue for customer>

...

&lt;total number of impressions>, &lt;total revenue>

The program should not take more than a minute to run on a modern laptop for each of the cases below using at most 8 GB of java heap space.


| **Sample problem 1**<br/>32356000<br/>Acme,2000000,200<br/>Lorem,3500000,400<br/>Ipsum,2300000,210<br/>Dolor,8000000,730<br/>SIT,10000000,1000<br/>Amet,1500000,160<br/>Mauris,1000000,100 | **Sample result 1**<br/>Acme,0,0,0<br/>Lorem,8,28000000,3200<br/>Ipsum,0,0,0<br/>Dolor,0,0,0<br/>SIT,0,0,0<br/>Amet,2,3000000,320<br/>Mauris,1,1000000,100<br/>32000000,3620  |
| --- | --- |
| **Sample problem 2**<br/>50000000<br/>Acme,1,0<br/>Lorem,2,2<br/>Ipsum,3,2<br/>Dolor,70000,71000<br/>Mauris,49000000,50000000 | **Sample result 2**<br/>Acme,0,0,0<br/>Lorem,10000,20000,20000<br/>Ipsum,0,0,0<br/>Dolor,14,980000,994000<br/>Mauris,1,49000000,50000000<br/>50000000,51014000  |
| **Sample problem 3**<br/>2000000000<br/>Acme,1000000,5000<br/>Lorem,2000000,9000<br/>Ipsum,3000000,20000 | **Sample result 3** <br/>Acme,2,2000000,10000<br/>Lorem,0,0,0<br/>Ipsum,666,1998000000,13320000<br/>2000000000,13330000 |