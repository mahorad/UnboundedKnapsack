package com.mahorad.knapsack;

import org.magicwerk.brownies.collections.BigList;

class FixList<T> extends BigList<T> {

    private int maxSize;

    FixList(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(T t) {
        super.add(t);
        if (size() > maxSize)
            remove(0);
        return true;
    }
}
