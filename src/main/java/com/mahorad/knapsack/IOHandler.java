package com.mahorad.knapsack;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class IOHandler {

    static final String output = "output";
    private static final String separator = ",";

    Specs toSpec(String filename) {
        List<String> lines = getLines(filename);
        int capacity = Integer.parseInt(lines.remove(0));
        Specs specs = new Specs(capacity);
        lines.forEach(line -> specs.add(parseItem(line)));
        return specs;
    }

    private List<String> getLines(String filename) {
        Path path = Paths.get(filename);
        try (BufferedReader br = Files.newBufferedReader(path)) {
            return br.lines().collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private Item parseItem(String line) {
        if (line == null || line.isEmpty())
            throw new IllegalArgumentException("invalid input line");
        String[] content = line.split(separator);
        int weight = Integer.parseInt(content[1].trim());
        int value = Integer.parseInt(content[2].trim());
        return new Item(content[0].trim(), weight, value);
    }

    void toFile(Specs specs) {
        Path path = Paths.get(output);
        try (BufferedWriter writer = Files.newBufferedWriter(path))
        {
            writer.write(specs.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
