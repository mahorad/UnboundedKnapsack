package com.mahorad.knapsack;

class Item implements Comparable<Item> {

    final String name;
    final int weight;
    final int value;
    final double ratio;

    Item(String name, int weight, int value) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException();
        if (weight < 0 || value < 0)
            throw new IllegalArgumentException();
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.ratio = (double) value / (double) weight;
    }

    @Override
    public int compareTo(Item o) {
        if (o == null)
            throw new IllegalArgumentException();
        if (ratio < o.ratio) return 1;
        if (ratio == o.ratio) {
            return weight - o.weight;
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        if (weight != item.weight) return false;
        if (value != item.value) return false;
        return name.equals(item.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + weight;
        result = 31 * result + value;
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s w:%d v:%d", name, weight, value);
    }

}
