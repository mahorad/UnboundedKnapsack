package com.mahorad.knapsack;

import java.lang.reflect.Field;

class Logic {

    private final FixList<Node> nodes;
    private int divisor;
    private final Specs specs;

    Logic(Specs specs) {
        this.specs = specs;
        calculateDivisor();
        int size = specs.maxWeight / divisor;
        nodes = new FixList<>(size);
        nodes.add(new Node());
    }

    void apply() {
        int upperBound;
        Node lastNode = nodes.get(0);
        Node thisNode;
        int at = -1;
        for(int size = divisor; size <= specs.capacity; size += divisor)
        {
            thisNode = new Node(lastNode);
            for(int itemIndex = 0; itemIndex < specs.items.size(); itemIndex++)
            {
                Item item = specs.items.get(itemIndex);
                if (item.weight > size) continue;
                int n = normalizeByMaxWeight(size);
                int index = (n - item.weight) / divisor;
                Node oldNode = nodes.get(index);
                upperBound = oldNode.value + item.value;
                if (upperBound <= thisNode.value) continue;
                at = itemIndex;
                thisNode = new Node(oldNode);
                thisNode.weight += item.weight;
                thisNode.value = upperBound;
            }
            addCount(thisNode, at);
            nodes.add(thisNode);
            lastNode = thisNode;
            at = -1;
        }
        specs.result = nodes.get(nodes.size() - 1);
    }

    private void addCount(Node node, int index) {
        if (node == null || index < 0) return;
        try {
            final Field itemField = Node.fields[index];
            int itemWeight = itemField.getInt(node);
            itemField.setInt(node, itemWeight + 1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private int normalizeByMaxWeight(int size) {
        return (size > specs.maxWeight)
                ? size - (size - specs.maxWeight)
                : size;
    }

    private void calculateDivisor() {
        Integer[] weights = specs.items
                .stream()
                .map(item -> item.weight)
                .toArray(Integer[]::new);
        divisor = gcd(weights);
    }

    static int gcd(Integer... numbers) {
        int result = numbers[0];
        for(int i = 1; i < numbers.length; i++)
            result = gcd(result, numbers[i]);
        return result;
    }

    private static int gcd(int a, int b) {
        if(a == 0) return b;
        if(b == 0) return a;
        if(a > b)  return gcd(b, a % b);
        return gcd(a, b % a);
    }

}