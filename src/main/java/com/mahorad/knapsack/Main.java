package com.mahorad.knapsack;

public class Main {

    public static void main(String[] args) {
        IOHandler ioHandler = new IOHandler();
        Specs specs = ioHandler.toSpec(args[0]);
//        Specs specs = ioHandler.toSpec("input_2");
        System.out.println("processing started, please wait...");
        long start = System.currentTimeMillis();
        new Logic(specs).apply();
        System.out.println("took " + (System.currentTimeMillis() - start) + " ms");
        ioHandler.toFile(specs);
        System.out.println("============= RESULTS ============\n" + specs.toString());
    }
}
