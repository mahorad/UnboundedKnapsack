package com.mahorad.knapsack;

import java.lang.reflect.Field;

class Node {

    int weight;
    int value;

    public int item0 = 0;
    public int item1 = 0;
    public int item2 = 0;
    public int item3 = 0;
    public int item4 = 0;
    public int item5 = 0;
    public int item6 = 0;

    static final Field[] fields;

    static {
        fields = new Field[7];
        for (int i = 0; i < 7; i++) {
            try {
                fields[i] = Node.class.getField("item" + String.valueOf(i));
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    Node() {
    }

    Node(Node parent) {
        if (parent == null)
            throw new IllegalArgumentException();
        value = parent.value;
        weight = parent.weight;
        setItems(parent);
    }

    private void setItems(Node parent) {
        this.item0 = parent.item0;
        this.item1 = parent.item1;
        this.item2 = parent.item2;
        this.item3 = parent.item3;
        this.item4 = parent.item4;
        this.item5 = parent.item5;
        this.item6 = parent.item6;
    }

    int getCount(int index) {
        if (index < 0 || index > 6) return -1;
        try {
            return fields[index].getInt(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public String toString() {
        return String.format("w:%d v:%d", weight, value);
    }
}