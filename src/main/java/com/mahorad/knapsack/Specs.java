package com.mahorad.knapsack;

import java.util.ArrayList;
import java.util.List;

class Specs {

    private static final String format = "%s,%d,%d,%d\n";

    final int capacity;
    final List<Item> items = new ArrayList<>();
    private final List<Item> originalItems = new ArrayList<>();

    private int minWeight = Integer.MAX_VALUE;
    int maxWeight = Integer.MIN_VALUE;

    Node result;

    Specs(int capacity) {
        this.capacity = capacity;
    }

    boolean add(Item item) {
        if (item == null)
            return false;
        originalItems.add(item);
        if (item.value <= 0) return false;
        if (item.weight > capacity)
            return false;
        minWeight = Math.min(minWeight, item.weight);
        maxWeight = Math.max(maxWeight, item.weight);
        return items.add(item);
    }

    @Override
    public String toString() {
        String content = "";
        for (Item item : originalItems) {
            if (items.contains(item)) {
                final int index = items.indexOf(item);
                final int count = result.getCount(index);
                final int weight = count * item.weight;
                final int value = count * item.value;
                content += String.format(format, item.name, count, weight, value);
            } else
                content += String.format(format, item.name, 0, 0, 0);
        }
        content += String.format("%d,%d", result.weight, result.value);
        return content;
    }
}