package com.mahorad.knapsack;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IOHandlerTest {

    @Test
    public void toSpec_MainTestCase1_ValidLogic() {
        IOHandler ioHandler = new IOHandler();
        Specs specs = ioHandler.toSpec("input_1");

        assertEquals(32356000, specs.capacity);
        assertEquals(7, specs.items.size());
    }

    @Test
    public void toSpec_MainTestCase2_ValidLogic() {
        IOHandler ioHandler = new IOHandler();
        final Specs specs = ioHandler.toSpec("input_2");

        assertEquals(50000000, specs.capacity);
        assertEquals(4, specs.items.size());
    }

    @Test
    public void toSpec_MainTestCase3_ValidLogic() {
        IOHandler ioHandler = new IOHandler();
        final Specs specs = ioHandler.toSpec("input_3");

        assertEquals(2000000000, specs.capacity);
        assertEquals(3, specs.items.size());
    }

    @Test
    public void toSpec_Input11_ValidLogic() {
        IOHandler ioHandler = new IOHandler();
        final Specs specs = ioHandler.toSpec("input_11");

        assertEquals(7, specs.capacity);
        assertEquals(5, specs.items.size());
    }

    @Test
    public void toSpec_Input12_ValidLogic() {
        IOHandler ioHandler = new IOHandler();
        final Specs specs = ioHandler.toSpec("input_12");

        assertEquals(10, specs.capacity);
        assertEquals(4, specs.items.size());
    }

    @Test
    public void toSpec_Input13_ValidLogic() {
        IOHandler ioHandler = new IOHandler();
        final Specs specs = ioHandler.toSpec("input_13");

        assertEquals(11, specs.capacity);
        assertEquals(3, specs.items.size());
    }

    @Test
    public void toSpec_Input14_ValidLogic() {
        IOHandler ioHandler = new IOHandler();
        final Specs specs = ioHandler.toSpec("input_14");

        assertEquals(105, specs.capacity);
        assertEquals(3, specs.items.size());
    }
}