package com.mahorad.knapsack;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ItemTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullName_ThrowsException() {
        new Item(null, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_EmptyName_ThrowsException() {
        new Item("", 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NegativeWight_ThrowsException() {
        new Item("company", -1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NegativeValue_ThrowsException() {
        new Item("company", 1, -1);
    }

    @Test
    public void ratio_ValueToWeightRatio() {
        Item item = new Item("a", 8, 24);
        assertEquals(3, item.ratio, Double.MIN_VALUE);
    }

    @Test
    public void equals_EqualsNull_ReturnsFalse() {
        Item item = new Item("a", 1, 1);
        assertFalse(item.equals(null));
    }

    @Test
    public void equals_EqualObject_ReturnsTrue() {
        Item item1 = new Item("a", 1, 1);
        Item item2 = new Item("a", 1, 1);
        assertTrue(item1.equals(item2));
        assertTrue(item2.equals(item1));
    }

    @Test
    public void equals_SameInstance_ReturnsTrue() {
        Item item1 = new Item("a", 1, 1);
        assertTrue(item1.equals(item1));
    }

    @Test
    public void equals_DifferentNames_ReturnsFalse() {
        Item item1 = new Item("a", 1, 1);
        Item item2 = new Item("b", 1, 1);
        assertFalse(item1.equals(item2));
    }

    @Test
    public void equals_DifferentWeights_ReturnsFalse() {
        Item item1 = new Item("a", 1, 1);
        Item item2 = new Item("a", 2, 1);
        assertFalse(item1.equals(item2));
    }

    @Test
    public void equals_DifferentValues_ReturnsFalse() {
        Item item1 = new Item("a", 1, 1);
        Item item2 = new Item("a", 1, 2);
        assertFalse(item1.equals(item2));
    }

    @Test
    public void hashCode_EqualsObjects_EqualsHashCodes() {
        Item item1 = new Item("a", 1, 1);
        Item item2 = new Item("a", 1, 1);
        assertEquals(item1.hashCode(), item2.hashCode());
    }

    @Test(expected = IllegalArgumentException.class)
    public void compareTo_Null_ThrowsException() {
        Item item1 = new Item("a", 33333333, 1);
        Item item2 = null;
        item1.compareTo(item2);
    }

    @Test
    public void compareTo_MoreRatio_ReturnsPlusOne() {
        Item item1 = new Item("a", 33333333, 1);
        Item item2 = new Item("b", 33333334, 1);
        assertEquals(-1, item1.compareTo(item2));
    }

    @Test
    public void compareTo_EqualRatiosEqualWeights_ReturnsZero() {
        Item item1 = new Item("a", 33333333, 1);
        Item item2 = new Item("b", 33333333, 1);
        assertEquals(item1.ratio, item2.ratio, Double.MIN_VALUE);
        assertEquals(0, item1.compareTo(item2));
    }

    @Test
    public void compareTo_EqualRatiosLessWeight_ReturnsPlusOne() {
        Item item1 = new Item("a", 33333333, 1);
        Item item2 = new Item("b", 66666666, 2);
        assertEquals(item1.ratio, item2.ratio, Double.MIN_VALUE);
        assertTrue(item1.compareTo(item2) < 0);
    }

    @Test
    public void compareTo_EqualRatiosMoreWeight_ReturnsMinusOne() {
        Item item1 = new Item("b", 66666666, 2);
        Item item2 = new Item("a", 33333333, 1);
        assertEquals(item1.ratio, item2.ratio, Double.MIN_VALUE);
        assertTrue(item1.compareTo(item2) > 0);
    }

    @Test
    public void sortByRatio_UnsortedListOfItems_ItemsAreSorted() {
        final Item a = new Item("a", 1, 1);
        final Item b = new Item("b", 2, 10);
        final Item c = new Item("c", 5, 12);
        final Item d = new Item("d", 3, 15);
        final Item e = new Item("e", 6, 31);
        final Item f = new Item("f", 3, 17);

        List<Item> items = new ArrayList<Item>() {{
            add(a);
            add(b);
            add(c);
            add(d);
            add(e);
            add(f);
        }};

        Collections.sort(items);
        assertEquals(items.get(0), f);
        assertEquals(items.get(1), e);
        assertEquals(items.get(2), b);
        assertEquals(items.get(3), d);
        assertEquals(items.get(4), c);
        assertEquals(items.get(5), a);
    }
}