package com.mahorad.knapsack;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LogicTest {

    @Test
    public void main_TestCase_1() {
        final Specs specs = new IOHandler().toSpec("input_1");
        new Logic(specs).apply();

        assertEquals(3620, specs.result.value, Double.MIN_VALUE);
        assertEquals(32000000, specs.result.weight, Double.MIN_VALUE);

        assertEquals(8, specs.result.item1);
        assertEquals(2, specs.result.item5);
        assertEquals(1, specs.result.item6);
    }

    @Test
    public void main_TestCase_2() {
        final Specs specs = new IOHandler().toSpec("input_2");
        new Logic(specs).apply();

        assertEquals(51014000, specs.result.value, Double.MIN_VALUE);
        assertEquals(50000000, specs.result.weight, Double.MIN_VALUE);

        assertEquals(10000, specs.result.item0);
        assertEquals(14, specs.result.item2);
        assertEquals(1, specs.result.item3);
    }

    @Test
    public void main_TestCase_3() {
        final Specs specs = new IOHandler().toSpec("input_3");
        new Logic(specs).apply();

        assertEquals(13330000, specs.result.value, Double.MIN_VALUE);
        assertEquals(2000000000, specs.result.weight, Double.MIN_VALUE);

        assertEquals(2, specs.result.item0);
        assertEquals(666, specs.result.item2);
    }

    @Test
    public void input11() {
        final Specs specs = new IOHandler().toSpec("input_11");
        new Logic(specs).apply();

        assertEquals(37, specs.result.value, Double.MIN_VALUE);
        assertEquals(7, specs.result.weight, Double.MIN_VALUE);

        assertEquals(2, specs.result.item1);
        assertEquals(1, specs.result.item4);
    }

    @Test
    public void input12() {
        final Specs specs = new IOHandler().toSpec("input_12");
        new Logic(specs).apply();

        assertEquals(51, specs.result.value, Double.MIN_VALUE);
        assertEquals(9, specs.result.weight, Double.MIN_VALUE);

        assertEquals(3, specs.result.item3);
    }

    @Test
    public void input13() {
        final Specs specs = new IOHandler().toSpec("input_13");
        new Logic(specs).apply();

        assertEquals(38, specs.result.value, Double.MIN_VALUE);
        assertEquals(11, specs.result.weight, Double.MIN_VALUE);

        assertEquals(1, specs.result.item0);
        assertEquals(2, specs.result.item1);

    }

    @Test
    public void input14() {
        final Specs specs = new IOHandler().toSpec("input_14");
        new Logic(specs).apply();

        assertEquals(382, specs.result.value, Double.MIN_VALUE);
        assertEquals(105, specs.result.weight, Double.MIN_VALUE);

        assertEquals(4, specs.result.item2);
        assertEquals(1, specs.result.item0);
    }

    @Test
    public void gcd_BigNumber() {
        final int gcd = Logic.gcd(2000000, 3500000, 2300000, 8000000, 10000000, 1500000, 1000000);
        assertEquals(100000, gcd);
    }

    @Test
    public void gcd_SmallNumber() {
        final int gcd = Logic.gcd(10, 11, 12);
        assertEquals(1, gcd);
    }
}